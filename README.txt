Registration Migrate
dpi@d.o - http://danielph.in

Migrates data from Signup 6.x to Registration 7.x.
Copyright (C) 2012 Daniel Phin

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## Instructions

  1. Create a registration type (admin/structure/registration_types)
  2. (optional) Add field to newly created registration type of type List(text)
     or other type. If creating with List(text), then add this to 
     `Allowed values list`: 
     
         0|Did Not Attend
         1|Attended
     
  3. Attach Registration fields to content types which Registrations are
     associated with.
  4. Migrate Signup data to Registration (admin/structure/signup-migrate)

## Other

  * __Force creation of per-entity registration settings.__
    This option will create registration settings for entities who have
    signups associated with them, but for some reason is missing the settings.
    This option will not necessarily open an entity for new registrations.  

## Migrated Data:

### {signup} to {registration_entity}

_Registrations settings per entity._

  * {signup}.nid =>                   {registration_entity}.entity_id
  * {signup}.close_signup_limit =>    {registration_entity}.capacity
  * {signup}.status =>                {registration_entity}.status
  * {signup}.send_reminder =>         {registration_entity}.send_reminder
  * {signup}.reminder_email =>        {registration_entity}.reminder_template (process)
  * {signup}.close_in_advance_time => None yet. Blocked by [#1316236]. (Note: Convert to absolute date)

### {signup_log} to {registration}

_Registrations per user per entity._

  * _user choice_ =>                    {registration}.type
  * {signup_log}.nid =>                 {registration}.entity_id
  * {signup_log}.uid =>                 {registration}.author_uid
  * _get from user_ =>                  {registration}.mail
  * {signup_log}.count_towards_limit => {registration}.count
  * {signup_log}.signup_time =>         {registration}.created

## Data that is lost (or will/cannot be migrated):

  * {signup}.forwarding_email
  * {signup}.user_reg_form
  * {signup}.reminder_days_before
  * {signup}.send_confirmation
  * {signup}.confirmation_email
  * {signup_log}.anon_mail
  * {signup_log}.attended (to bool field?)
  * {signup_log}.form_data (TODO: allow hook)
